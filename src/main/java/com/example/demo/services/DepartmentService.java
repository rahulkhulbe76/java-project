package com.example.demo.services;

import com.example.demo.models.Department;
import java.util.List;

public interface DepartmentService {
    List<Department> getDepartments();
    Department getDepartmentById(int Id);
    Department insert(Department Department);
    void updateDepartment (int id, Department Department);
    void deleteDepartment(int DepartmentId);
}
