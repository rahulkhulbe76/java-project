package com.example.demo.services;
import com.example.demo.Response.BillMessage;
import org.springframework.stereotype.Service;
import com.example.demo.models.Patient;
import com.example.demo.repository.PatientRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PatientServiceImpl implements PatientService {
    PatientRepository patientRepository;

    public PatientServiceImpl(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    @Override
    public List<Patient> getPatients()
    {
        List<Patient> patients=new ArrayList<>();
        patients.addAll(patientRepository.findAll());
        return patients;
    }
    @Override
    public Patient getPatientById(int id) {
        return patientRepository.findById(id).get();
    }
    @Override
    public Patient insert(Patient patient){
        return patientRepository.save(patient);
    }
    @Override
    public void updatePatient (int id, Patient patient){
        Patient patientFromDb=patientRepository.findById(id).get();
        System.out.println(patientFromDb);
        patientFromDb.setPatientName(patient.getPatientName());
        patientFromDb.setPatientContact(patient.getPatientContact());
        patientFromDb.setNoOfDays(patient.getNoOfDays());
        patientFromDb.setDisease(patient.getDisease());
        patientFromDb.setSeverity(patient.getSeverity());
        patientRepository.save(patientFromDb);
    }
    @Override
    public void deletePatient(int patientId){
           patientRepository.deleteById(patientId);
    }

    @Override
    public BillMessage generateBill(int patientId) {
      Optional<Patient> patient = patientRepository.findById(patientId);
      int bill = patient.get().getNoOfDays()*1500;
      return new BillMessage("Please pay a bill of: " + bill );
    }

}


