package com.example.demo.services;

import com.example.demo.models.Department;
import com.example.demo.repository.DepartmentRepository;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    DepartmentRepository departmentRepository;

    public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Override
    public List<Department> getDepartments()
    {
        List<Department> departments=new ArrayList<>();
        departments.addAll(departmentRepository.findAll());
        return departments;
    }
    @Override
    public Department getDepartmentById(int id) {
        return departmentRepository.findById(id).get();
    }
    @Override
    public Department insert(Department department){
        return departmentRepository.save(department);
    }
    @Override
    public void updateDepartment (int id, Department department){
        Department departmentFromDb=departmentRepository.findById(id).get();
        System.out.println(departmentFromDb);
        departmentFromDb.setDeptName(department.getDeptName());
        departmentFromDb.setAvailableDocs(department.getAvailableDocs());
        departmentFromDb.setBedsAvailable(department.getBedsAvailable());
        departmentFromDb.setIcuBedsAvailable(department.getIcuBedsAvailable());
    }
    @Override
    public void deleteDepartment(int DepartmentId){
        departmentRepository.deleteById(DepartmentId);
    }
}
