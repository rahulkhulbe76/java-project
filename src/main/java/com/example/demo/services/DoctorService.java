package com.example.demo.services;
import com.example.demo.models.Doctor;

import javax.print.Doc;
import java.util.List;

public interface DoctorService {
    List<Doctor> getDoctors();
    Doctor getDoctorById(int Id);
    Doctor insert(Doctor Doctor);
    void updateDoctor (int id, Doctor Doctor);
    void deleteDoctor(int DoctorId);
    Doctor getDoctorByDisease(String Disease);
}