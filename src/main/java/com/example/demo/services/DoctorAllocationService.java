package com.example.demo.services;

import com.example.demo.models.DoctorAllocation;


import java.util.List;

public interface DoctorAllocationService {
    List<DoctorAllocation> getDoctorAllocations();
    DoctorAllocation getDoctorAllocationById(int id);
    DoctorAllocation insert(DoctorAllocation doctorAllocation);
    void updateAllocation (int id, DoctorAllocation doctorAllocation);
    void deleteAllocation(int DocId);
}
