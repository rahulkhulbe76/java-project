package com.example.demo.services;
import com.example.demo.Response.BillMessage;
import com.example.demo.models.Patient;
import java.util.List;

public interface PatientService {
    List<Patient> getPatients();
    Patient getPatientById(int Id);
    Patient insert(Patient patient);
    void updatePatient (int id, Patient patient);
    void deletePatient(int patientId);
    BillMessage generateBill(int patientId);
}
