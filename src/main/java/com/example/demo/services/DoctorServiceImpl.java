package com.example.demo.services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.models.Doctor;
import com.example.demo.repository.DoctorRepository;
import java.util.ArrayList;
import java.util.List;

@Service
public class DoctorServiceImpl implements DoctorService {
    @Autowired
    DoctorRepository doctorRepository;
    public DoctorServiceImpl(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    @Override
    public List<Doctor> getDoctors()
    {
        List<Doctor> Doctors=new ArrayList<>();
        Doctors.addAll(doctorRepository.findAll());
        return Doctors;
    }
    @Override
    public Doctor getDoctorById(int id) {
        return doctorRepository.findById(id).get();
    }
    @Override
    public Doctor insert(Doctor doctor){
        return doctorRepository.save(doctor);
    }
    @Override
    public void updateDoctor (int id, Doctor Doctor){
        Doctor doctorFromDb=doctorRepository.findById(id).get();
        System.out.println(doctorFromDb);
        doctorFromDb.setDoctorName(Doctor.getDoctorName());
        doctorFromDb.setdeptName(Doctor.getdeptName());
        doctorFromDb.setMaxThresholdPatients(Doctor.getMaxThresholdPatients());
    }
    @Override
    public void deleteDoctor(int DoctorId){
        doctorRepository.deleteById(DoctorId);
    }
    @Override
    public Doctor getDoctorByDisease(String Disease)
    {
        Doctor doctor = new Doctor();
        List<Doctor> doctors = doctorRepository.findAll();
        for (Doctor s: doctors) {
            if(s.getdeptName().equals(Disease) && s.getMaxThresholdPatients()>0)
            {
                doctor=s;
                doctor.setMaxThresholdPatients(doctor.getMaxThresholdPatients()-1);
                break;
            }
        }
        return doctor;
    }
}


