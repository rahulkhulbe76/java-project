package com.example.demo.services;

import com.example.demo.models.DoctorAllocation;
import com.example.demo.repository.DoctorAllocationRepository;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class DoctorAllocationServiceImpl implements DoctorAllocationService{
    DoctorAllocationRepository doctorAllocationRepository;

    public DoctorAllocationServiceImpl(DoctorAllocationRepository doctorAllocationRepository) {
        this.doctorAllocationRepository = doctorAllocationRepository;
    }

    @Override
    public List<DoctorAllocation> getDoctorAllocations()
    {
        List<DoctorAllocation> DoctorAllocations=new ArrayList<>();
        DoctorAllocations.addAll(doctorAllocationRepository.findAll());
        return DoctorAllocations;
    }

    @Override
    public DoctorAllocation getDoctorAllocationById(int id){
        return doctorAllocationRepository.findById(id).get();
    }

    @Override
    public DoctorAllocation insert(DoctorAllocation doctorAllocation){
        return doctorAllocationRepository.save(doctorAllocation);
    }

    @Override
    public void updateAllocation (int id, DoctorAllocation doctorAllocation){
        DoctorAllocation AllocationFromDb= doctorAllocationRepository.findById(id).get();
        System.out.println(AllocationFromDb);
        AllocationFromDb.setDoctorId(doctorAllocation.getDoctorId());
        AllocationFromDb.setPatientId(doctorAllocation.getPatientId());
    }
    @Override
    public void deleteAllocation(int DocId){
        doctorAllocationRepository.deleteById(DocId);
    }
}
