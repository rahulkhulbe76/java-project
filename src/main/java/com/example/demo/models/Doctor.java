package com.example.demo.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Doctor {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int DoctorId;
    @Column
    private String deptName;
    @Column
    private String DoctorName;
    @Column
    private int MaxThresholdPatients;

    public int getDoctorId() {
        return DoctorId;
    }

    public void setDoctorId(int doctorId) {
        DoctorId = doctorId;
    }

    public String getdeptName() {
        return deptName;
    }

    public void setdeptName(String deptName) {
        deptName = deptName;
    }

    public String getDoctorName() {
        return DoctorName;
    }

    public void setDoctorName(String doctorName) {
        DoctorName = doctorName;
    }

    public int getMaxThresholdPatients() {
        return MaxThresholdPatients;
    }

    public void setMaxThresholdPatients(int maxThresholdPatients) {
        MaxThresholdPatients = maxThresholdPatients;
    }
}
