package com.example.demo.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Department {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int DeptId;
    @Column
    private String deptName;
    @Column
    private int AvailableDocs;
    @Column
    private int BedsAvailable;
    @Column
    private int IcuBedsAvailable;

    public int getIcuBedsAvailable() {
        return IcuBedsAvailable;
    }

    public void setIcuBedsAvailable(int icuBedsAvailable) {
        IcuBedsAvailable = icuBedsAvailable;
    }

    public int getDeptId() {
        return DeptId;
    }

    public void setDeptId(int deptId) {
        DeptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        deptName = deptName;
    }

    public int getAvailableDocs() {
        return AvailableDocs;
    }

    public void setAvailableDocs(int availableDocs) {
        AvailableDocs = availableDocs;
    }

    public int getBedsAvailable() {
        return BedsAvailable;
    }

    public void setBedsAvailable(int bedsAvailable) {
        BedsAvailable = bedsAvailable;
    }


}
