package com.example.demo.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Patient {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int PatientId;
    @Column
    private String PatientName;
    @Column
    private String  PatientContact;
    @Column
    private int NoOfDays;
    @Column
    private String Disease;
    @Column
    private String Severity;

    public int getPatientId() {
        return PatientId;
    }

    public void setPatientId(int patientId) {
        PatientId = patientId;
    }

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public String getPatientContact() {
        return PatientContact;
    }

    public void setPatientContact(String patientContact) {
        PatientContact = patientContact;
    }

    public int getNoOfDays() {
        return NoOfDays;
    }

    public void setNoOfDays(int noOfDays) {
        NoOfDays = noOfDays;
    }

    public String getDisease() {
        return Disease;
    }

    public void setDisease(String disease) {
        Disease = disease;
    }

    public String getSeverity() {
        return Severity;
    }

    public void setSeverity(String severity) {
        Severity = severity;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "PatientId=" + PatientId +
                ", PatientName='" + PatientName + '\'' +
                ", PatientContact='" + PatientContact + '\'' +
                ", NoOfDays=" + NoOfDays +
                ", Disease='" + Disease + '\'' +
                ", Severity='" + Severity + '\'' +
                '}';
    }

}
