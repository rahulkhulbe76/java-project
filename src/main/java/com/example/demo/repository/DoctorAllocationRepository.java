package com.example.demo.repository;

import com.example.demo.models.DoctorAllocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorAllocationRepository extends JpaRepository<DoctorAllocation,Integer> {
}
