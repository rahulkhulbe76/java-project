package com.example.demo.controller;

import com.example.demo.models.DoctorAllocation;
import com.example.demo.models.Patient;
import com.example.demo.services.DoctorAllocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/v1/DoctorAllocation")
public class DoctorAllocationController {
    @Autowired
    DoctorAllocationService doctorAllocationService;

    public DoctorAllocationController(DoctorAllocationService doctorAllocationService) {
        this.doctorAllocationService = doctorAllocationService;
    }

    @GetMapping
    public ResponseEntity<List<DoctorAllocation>> getAllDoctorAllocation() {
        List<DoctorAllocation> doctorAllocations = doctorAllocationService.getDoctorAllocations();
        return new ResponseEntity<>(doctorAllocations, HttpStatus.OK);
    }

    @GetMapping({"/{doctorAllocationId}"})
    public ResponseEntity<DoctorAllocation> getdoctorAllocation(@PathVariable int doctorAllocationId) {
        return new ResponseEntity(doctorAllocationService.getDoctorAllocationById(doctorAllocationId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<DoctorAllocation> savePatient(@RequestBody DoctorAllocation doctorAllocation) {

        DoctorAllocation doctorAllocation1 = doctorAllocationService.insert(doctorAllocation);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("patient","api/v1/DoctorAllocation"+doctorAllocation1.getDoctorId());
        return new ResponseEntity<>(doctorAllocation1, httpHeaders, HttpStatus.CREATED);
    }

    @PutMapping({"/{doctorAllocationId}"})
    public ResponseEntity<Patient> updateDoctorAllocation(@PathVariable("doctorAllocationId") int doctorAllocationId, @RequestBody DoctorAllocation doctorAllocation) {
        doctorAllocationService.updateAllocation(doctorAllocationId, doctorAllocation);
        return new ResponseEntity(doctorAllocationService.getDoctorAllocationById(doctorAllocationId), HttpStatus.OK);
    }

    @DeleteMapping({"delete/{doctorAllocationId}"})
    public ResponseEntity<Patient> deletePatient(@PathVariable("doctorAllocationId") int doctorAllocationId) {
        doctorAllocationService.deleteAllocation(doctorAllocationId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
