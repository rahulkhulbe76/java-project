package com.example.demo.controller;
import com.example.demo.Response.BillMessage;
import com.example.demo.models.*;
import com.example.demo.repository.DoctorAllocationRepository;
import com.example.demo.repository.DoctorRepository;
import com.example.demo.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/Patient")
public class PatientController {
    @Autowired
    PatientService patientService;
    @Autowired
    DoctorServiceImpl doctorService;
    @Autowired
    DoctorRepository doctorRepository;
    @Autowired
    DoctorAllocationServiceImpl doctorAllocationService;
    @Autowired
    DoctorAllocationRepository doctorAllocationRepository;


    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping
    public ResponseEntity<List<Patient>> getAllPatients() {
        List<Patient> patients = patientService.getPatients();
        return new ResponseEntity<>(patients, HttpStatus.OK);
    }

    @GetMapping({"/{patientId}"})
    public ResponseEntity<Patient> getPatient(@PathVariable int patientId) {
        return new ResponseEntity<>(patientService.getPatientById(patientId), HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Patient> savePatient(@RequestBody Patient patient) {

        Patient patient1 = patientService.insert(patient);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("patient","api/v1/Patient"+patient1.getPatientId());
        DoctorAllocation doctorAllocation = new DoctorAllocation();
        Doctor doctor = new Doctor();
        doctor = doctorService.getDoctorByDisease(patient.getDisease());
        doctorAllocation.setDoctorId(doctor.getDoctorId());
        doctorAllocation.setPatientId(patient.getPatientId());
        doctorAllocationRepository.save(doctorAllocation);
        return new ResponseEntity<>(patient1, httpHeaders, HttpStatus.CREATED);
    }

    @PutMapping({"/{patientId}"})
    public ResponseEntity<Patient> updatePatient(@PathVariable("patientId") int patientId, @RequestBody Patient patient) {
        patientService.updatePatient(patientId, patient);
        return new ResponseEntity<>(patientService.getPatientById(patientId), HttpStatus.OK);
    }

    @DeleteMapping({"/delete/{patientId}"})
    public ResponseEntity<Patient> deletePatient(@PathVariable("patientId") int patientId) {
        patientService.deletePatient(patientId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping({"/bill/{patientId}"})
    public BillMessage getBill(@PathVariable ("patientId") int patientId){
       BillMessage message = patientService.generateBill(patientId);
       return message;
    }
   /*@GetMapping(value = "/disease/{Disease}")
    public Doctor getDoctorByDisease(@PathVariable("Disease") String Disease){
       Doctor doctor = new Doctor();
       doctor = doctorService.getDoctorByDisease(Disease);
       return doctor;
    }*/
}
