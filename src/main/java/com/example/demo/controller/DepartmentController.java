package com.example.demo.controller;

import com.example.demo.models.Department;
import com.example.demo.models.Patient;
import com.example.demo.services.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/Dept")
public class DepartmentController {
    @Autowired
    DepartmentService departmentService;

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping
    public ResponseEntity<List<Department>> getAllDepartment() {
        List<Department> departments = departmentService.getDepartments();
        return new ResponseEntity<>(departments, HttpStatus.OK);
    }

    @GetMapping({"/{DepartmentId}"})
    public ResponseEntity<Department> getDepartment(@PathVariable int departmentId) {
        return new ResponseEntity<>(departmentService.getDepartmentById(departmentId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Department> savePatient(@RequestBody Department department) {

        Department department1 = departmentService.insert(department);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("patient","api/v1/Department"+department1.getDeptId());
        return new ResponseEntity<>(department1, httpHeaders, HttpStatus.CREATED);
    }

    @PutMapping({"/{departmentId}"})
    public ResponseEntity<Patient> updateDepartment(@PathVariable("departmentId") int departmentId, @RequestBody Department Department) {
        departmentService.updateDepartment(departmentId, Department);
        return new ResponseEntity(departmentService.getDepartmentById(departmentId), HttpStatus.OK);
    }

    @DeleteMapping({"delete/{departmentId}"})
    public ResponseEntity<Patient> deletePatient(@PathVariable("departmentId") int departmentId) {
        departmentService.deleteDepartment(departmentId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
