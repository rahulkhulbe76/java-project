package com.example.demo.controller;

import com.example.demo.models.Doctor;
import com.example.demo.models.Patient;
import com.example.demo.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/v1/Doctor")
public class DoctorController {
    @Autowired
    DoctorService doctorService;

    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping
    public ResponseEntity<List<Doctor>> getAllDoctor() {
        List<Doctor> doctors = doctorService.getDoctors();
        return new ResponseEntity<>(doctors, HttpStatus.OK);
    }

    @GetMapping({"/{doctorId}"})
    public ResponseEntity<Doctor> getDoctor(@PathVariable int doctorId) {
        return new ResponseEntity<>(doctorService.getDoctorById(doctorId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Doctor> savePatient(@RequestBody Doctor doctor) {

        Doctor doctor1 = doctorService.insert(doctor);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("patient","api/v1/Doctor"+doctor1.getDoctorId());
        return new ResponseEntity<>(doctor1, httpHeaders, HttpStatus.CREATED);
    }

    @PutMapping({"/{doctorId}"})
    public ResponseEntity<Patient> updateDoctor(@PathVariable("doctorId") int doctorId, @RequestBody Doctor doctor) {
        doctorService.updateDoctor(doctorId, doctor);
        return new ResponseEntity(doctorService.getDoctorById(doctorId), HttpStatus.OK);
    }

    @DeleteMapping({"delete/{DoctorId}"})
    public ResponseEntity<Patient> deletePatient(@PathVariable("doctorId") int doctorId) {
        doctorService.deleteDoctor(doctorId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
